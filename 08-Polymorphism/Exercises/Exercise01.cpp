//
// Created by mathe on 04/05/2021.
//

#include <iostream>
#include "Exercise01.h"
#include "../Classes/Accounts.h"

using namespace std;

void Exercise01()
{
    cout << "\n=======Pointers=========" << endl;

    auto *p1 = new Account();
    Account *p2 = new SavingsAccount();
    Account *p3 = new CheckingAccount();
    Account *p4 = new TrustAccount();

    p1->Withdraw(1000);
    p2->Withdraw(1000);
    p3->Withdraw(1000);
    p4->Withdraw(1000);

    delete p1;
    delete p2;
    delete p3;
    delete p4;
}

