//
// Created by mathe on 04/05/2021.
//

#include <iostream>
#include <vector>
#include "../Classes/Shapes.h"
#include "Exercise03.h"

using namespace std;

void Exercise03()
{
    Shape *circle = new Circle;
    circle->Draw();
    circle->Rotate();

    Shape *s1 = new Circle;
    Shape *s2 = new Square;
    Shape *s3 = new Line;

    vector<Shape *> shapes {s1, s2, s3};

    for(auto const p: shapes)
    {
        p->Draw();
    }

    delete circle;
    delete s1;
    delete s2;
    delete s3;

}
