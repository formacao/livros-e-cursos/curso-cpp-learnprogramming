//
// Created by mathe on 04/05/2021.
//

#include "Exercise02.h"
#include "../Classes/Accounts.h"

void DoWithdraw(Account &account, double amount)
{
    account.Withdraw(amount);
}

void Exercise02()
{
    Account account;
    Account &accountRef = account;
    accountRef.Withdraw(1000);

    TrustAccount trustAccount;
    accountRef = trustAccount;
    trustAccount.Withdraw(1000);

    Account a1;
    SavingsAccount a2;
    TrustAccount a3;
    CheckingAccount a4;

    DoWithdraw(a1, 1000);
    DoWithdraw(a2, 1000);
    DoWithdraw(a3, 1000);
    DoWithdraw(a4, 1000);
}
