//
// Created by mathe on 04/05/2021.
//
#include <iostream>
#include "../Classes/Accounts.h"
#include "Exercise04.h"

using namespace std;

void Exercise04()
{
    Account *account = new Account;
    Account *checking = new CheckingAccount;

    cout << *account << endl;
    cout << *checking << endl;

    delete account;
    delete checking;
}
