//
// Created by mathe on 04/05/2021.
//

#ifndef INC_08_POLYMORPHISM_IPRINTABLE_H
#define INC_08_POLYMORPHISM_IPRINTABLE_H

#include <iostream>
#include <string>

class IPrintable
{
public:
    virtual void Print(std::ostream &outputStream) const = 0;
};

std::ostream  &operator << (std::ostream &outputStream, const IPrintable &printable);

#endif //INC_08_POLYMORPHISM_IPRINTABLE_H
