#include <iostream>
#include "Exercises/Exercise01.h"
#include "Exercises/Exercise02.h"
#include "Exercises/Exercise03.h"
#include "Exercises/Exercise04.h"

using namespace std;

int main()
{
    auto option = 0;
    do
    {
        cout << "Select an option: " << endl;
        cout << "1 - Pointer Polymorphism" << endl;
        cout << "2 - Reference Polymorphism" << endl;
        cout << "3 - Abstract class" << endl;
        cout << "4 - Interfaces" << endl;
        cout << "0 - Exit" << endl;

        cin >> option;

        switch (option)
        {
            case 1:
                Exercise01();
                break;

            case 2:
                Exercise02();
                break;

            case 3:
                Exercise03();
                break;

            case 4:
                Exercise04();
                break;
        }

    }while(option != 0);

    return 0;
}
