//
// Created by mathe on 04/05/2021.
//

#ifndef INC_08_POLYMORPHISM_SHAPES_H
#define INC_08_POLYMORPHISM_SHAPES_H

#include <iostream>

using namespace std;

class Shape
{
public:
    virtual void Draw() = 0;
    virtual void Rotate() = 0;
    virtual ~Shape(){};
};

class OpenShape : public Shape
{
public:
    ~OpenShape() override{};
};

class ClosedShape : public Shape
{
public:
    ~ClosedShape() override{};
};

class Line : public OpenShape
{
public:
    void Draw() override {cout << "Drawing a line" << endl;}
    void Rotate() override {cout << "Rotating a line" << endl;}
    ~Line() override{};
};

class Circle: public ClosedShape
{
public:
    void Draw() override {cout << "Drawing a circle" << endl;}
    void Rotate() override{cout << "Rotating a circle" << endl;}
    ~Circle() override{};
};

class Square: public ClosedShape
{
public:
    void Draw() override {cout << "Drawing a square" << endl;}
    void Rotate() override{cout << "Rotating a square" << endl;}
    ~Square() override{};
};


#endif //INC_08_POLYMORPHISM_SHAPES_H
