//
// Created by mathe on 04/05/2021.
//

#include <iostream>
#include "Accounts.h"

using namespace std;

void Account::Withdraw(double amount)
{
    cout << "In Account::Withdraw" << endl;
}

Account::~Account()
{
    cout << "Account::Destructor" << endl;
}

void Account::Print(ostream &outputStream) const
{
    outputStream << "Account display";
}

void CheckingAccount::Withdraw(double amount)
{
    cout << "In CheckingAccount::Withdraw" << endl;
}

void CheckingAccount::Print(ostream &outputStream) const
{
    outputStream << "Checking display";
}

void SavingsAccount::Withdraw(double amount)
{
    cout << "In SavingsAccount::Withdraw" << endl;
}

void SavingsAccount::Print(ostream &outputStream) const
{
    outputStream << "Savings display";
}

void TrustAccount::Withdraw(double amount)
{
    cout << "In TrustAccount::Withdraw" << endl;
}

void TrustAccount::Print(ostream &outputStream) const
{
    outputStream << "Trust display";

}
