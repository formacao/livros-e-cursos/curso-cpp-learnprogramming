//
// Created by cicero.lacerda on 24/03/2021.
//

#include <iostream>
#include <vector>
#include "references.h"

using namespace std;

void Swap(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}

void References()
{
	int a {5}, b {18};
	
	cout << "a: " << a << " b: " << b << endl;
	Swap(a,b);
	cout << "a: " << a << " b: " << b << endl;
	
	cout << "\n--------------------------------------------" << endl;
	
	vector<string> stooges {"Larry", "Moe", "Curly"};
	
	for (auto str : stooges) str = "Funny"; //str is a copy
	for (auto str: stooges) cout << str << " ";
	cout << endl;
	
	cout << "\n-------------------------------------------" << endl;
	
	for(auto &str: stooges) str = "Funny"; //str is a reference, that is, not a copy
	for(auto str: stooges) cout << str << " ";
	cout << endl;
	
	for(auto const &str: stooges) cout << str << " ";
	cout << endl;
}