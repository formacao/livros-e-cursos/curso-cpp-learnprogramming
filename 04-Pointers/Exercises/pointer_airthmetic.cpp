//
// Created by mathe on 19/03/2021.
//
#include <iostream>
#include "pointer_airthmetic.h"

using namespace std;

void PointerArithmetic()
{
    int scores[] {100,95,89, 68 ,-1};
    int *scoresPtr {scores};

    while(*scoresPtr != -1)
    {
        cout <<*scoresPtr << endl;
        scoresPtr++;
    }

    cout << "\n----------------------------" << endl;
    scoresPtr = scores;
    while (*scoresPtr != -1)
        cout << *scoresPtr++ << endl;

    cout << "\n-----------------------------" << endl;
    string s1 {"Cicero"};
    string s2 {"Cicero"};
    string s3 {"Lacerda"};

    string *p1 {&s1};
    string *p2 {&s2};
    string *p3 {&s1};

    cout << boolalpha;
    cout << p1 << " == " << p2 << " : " << (p1 == p2) << endl;
    cout << p1 << " == " << p3 << " : " << (p1 == p3) << endl;

    cout << *p1 << " == " << *p2 << " : " << (*p1 == *p2) << endl;
    cout << *p1 << " == " << *p3 << " : " << (*p1 == *p3) << endl;

    p3 = &s3;
    cout << *p1 << " == " << *p3 << " : " << (*p1 == *p3) << endl;



}
