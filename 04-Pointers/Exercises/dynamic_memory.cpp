#include <iostream>
#include "dynamic_memory.h"

using namespace std;

void DynamicMemoryAllocation()
{
    int *intPtr {nullptr};
    intPtr = new int;
    cout << intPtr << endl;
    delete intPtr;

    size_t  size;
    double *tempPtr;

    cout << "How many temperatures?" << endl;
    cin >> size;

    tempPtr = new double[size];
    cout << tempPtr << endl;
    delete[] tempPtr;
}
