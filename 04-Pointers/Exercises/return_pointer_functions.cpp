//
// Created by cicero.lacerda on 24/03/2021.
//
#include <iostream>
#include "return_pointer_functions.h"

using namespace std;

int *CreateArray(size_t size, int defaultValue = 0);
void ShowArray(const int *array, size_t size);

void ReturnPointerFunctions()
{
	int *myArray {nullptr};
	size_t size;
	int initializeValue;
	
	cout << "\nHow many integers do you want to allocate? " << endl;
	cin >> size;
	cout << "\nWhat value would you like them initialized to? " << endl;
	cin >> initializeValue;
	cout << endl;
	
	myArray = CreateArray(size, initializeValue);
	cout << "\n--------------------------------------------" << endl;
	
	ShowArray(myArray, size);
	delete[] myArray;
}

int *CreateArray(size_t size, int defaultValue)
{
	auto *newStorage {new int[size]};
	for (auto i = 0; i < size ; ++i) *(newStorage + i) = defaultValue;
	return newStorage;
}

void ShowArray(const int *array, size_t size)
{
	for (auto i = 0; i < size; ++i) cout << *array++ << " ";
	cout << endl;
}