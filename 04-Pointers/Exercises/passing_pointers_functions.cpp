//
// Created by cicero.lacerda on 23/03/2021.
//
#include <iostream>
#include "passing_pointers_functions.h"

void doubleData(int *pInt);
void ShowValue(int value);
void DisplayArray(const int *array, int sentinel);

using namespace std;

void PassingPointersToFunctions()
{
	int value {10};
	int *valuePtr {nullptr};
	
	ShowValue(value);
	doubleData(&value);
	ShowValue(value);
	
	valuePtr = &value;
	doubleData(valuePtr);
	ShowValue(value);
	
	cout << "\n-----------------------------------" << endl;
	
	int scores[] {100,98,97,79,85,-1};
	cout << "Scores address: " << scores << endl;
	DisplayArray(scores, -1);
	cout << "Scores address: " << scores << endl;
	cout << endl;
}

void ShowValue(int value)
{ cout << "Value: " << value << endl; }

void doubleData (int *data)
{
	*data *= 2;
}

void DisplayArray(const int *array, int sentinel)
{
	while(*array != sentinel)
	{
		cout << *array++ << " ";
	}
	cout << endl;
}
