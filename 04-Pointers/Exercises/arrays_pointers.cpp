#include <iostream>
#include "arrays_pointers.h"

using namespace  std;

void ArraysAndPointers()
{
    int scores[] {100, 95, 89};
    int *scoresPtr {scores};

    cout << "Value scores " << scores << endl;
    cout << "Value of scoresPtr " << scoresPtr << endl;

    cout << "Array subscription notation------------------------" << endl;
    cout << scores[0] << endl;
    cout << scores[1] << endl;
    cout << scores[2] << endl;

    cout << "Pointer subscription notation---------------------------" << endl;
    cout << scoresPtr[0] << endl;
    cout << scoresPtr[1] << endl;
    cout << scoresPtr[2] << endl;

    cout << "Array offset notation-------------------------------" << endl;
    cout << *scores << endl;
    cout << *(scores + 1) << endl;
    cout << *(scores + 2) << endl;

    cout << "Pointer offset notation----------------------------------" << endl;
    cout << *scoresPtr << endl;
    cout << *(scoresPtr + 1) << endl;
    cout << *(scoresPtr + 2) << endl;

    cout << endl;
}

