#include <iostream>
#include <vector>
#include <string>
#include "dereferencing_pointers.h"

void DivideSection();

using namespace std;

void DereferencingPointers()
{
    int score{100};
    int *scorePtr{&score};
    cout << *scorePtr << endl;

    *scorePtr = 200;
    cout << *scorePtr << endl;
    cout << score << endl;

    DivideSection();
    double highTemp{100.7};
    double lowTemp{37.4};
    double *tempPtr{&highTemp};

    cout << *tempPtr << endl;
    tempPtr = &lowTemp;
    cout << *tempPtr << endl;

    DivideSection();
    string name{"Cícero"};
    string *namePtr{&name};

    cout << *namePtr << endl;
    name = "Lacerda";
    cout << *namePtr << endl;

    DivideSection();

    vector<string> stooges{"Larry", "Moe", "Cully"};
    vector<string> *vectorPtr{&stooges};

    cout << "First Stooge: " << (*vectorPtr).at(0) << endl;
    for (auto stooge : *vectorPtr)
    {
        cout << stooge << " ";
    }
    cout << endl;
    cout << endl;
}

void DivideSection()
{
    cout << "\n-------------------------------------------\n" << endl;
}
