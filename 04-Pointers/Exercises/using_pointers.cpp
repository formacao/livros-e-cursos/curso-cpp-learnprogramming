#include <iostream>
#include <vector>
#include "using_pointers.h"

#define nameof(x) #x

using namespace std;

void UsingPointers()
{
    int num{10};
    int *p{nullptr};

    cout << "The value of " << nameof(num) << " is " << num << endl;
    cout << "The size of " << nameof(num) << " is " << sizeof num << endl;
    cout << "The addrress of " << nameof(num) << " is " << &num << "\n" << endl;

    cout << "Value of " << nameof(p) << " is " << p << endl;
    cout << "Address of " << nameof(p) << " is " << &p << endl;
    cout << "The size of " << nameof(p) << " is " << sizeof p << "\n" << endl;

    int *p1;
    double *p2;
    unsigned long long *p3;
    vector<string> *p4;
    string *p5;

    cout << "The size of " << nameof(p1) << " is " << sizeof p1 << endl;
    cout << "The size of " << nameof(p2) << " is " << sizeof p2 << endl;
    cout << "The size of " << nameof(p3) << " is " << sizeof p3 << endl;
    cout << "The size of " << nameof(p4) << " is " << sizeof p4 << endl;
    cout << "The size of " << nameof(p5) << " is " << sizeof p5 << "\n" << endl;

    int score{100};
    int *scorePointer{&score};

    cout << "Value of " << nameof(score) << " is " << score << endl;
    cout << "Address of " << nameof(scorePointer) << " is " << &scorePointer << endl;
    cout << "Address of " << nameof(score) << " is " << &score << endl;
    cout << "Value of " << nameof(scorePointer) << " is " << scorePointer << endl;
}


