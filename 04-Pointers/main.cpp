#include <iostream>
#include "Exercises/using_pointers.h"
#include "Exercises/dereferencing_pointers.h"
#include "Exercises/dynamic_memory.h"
#include "Exercises/arrays_pointers.h"
#include "Exercises/pointer_airthmetic.h"
#include "Exercises/passing_pointers_functions.h"
#include "Exercises/return_pointer_functions.h"
#include "Exercises/references.h"

using namespace std;

int main()
{
    int selectedOption {0};
    do
    {
        cout << "Select one option:" << endl;
        cout << "1 - Using Pointers\n2 - Dereferencing Pointers\n3 - Dynamic Memory Allocation\n4 - Arrays and Pointers\n5 - Pointer Arithmetic\n6 - Passing Pointers to Functions" ;
        cout << "\n7 - Return Pointer from Functions\n8 - References\n0 - Exit" << endl;
        cin >> selectedOption;
        switch (selectedOption)
        {
            case 1:
                UsingPointers();
                break;
            case 2:
                DereferencingPointers();
                break;;
            case 3:
                DynamicMemoryAllocation();
                break;
            case 4:
                ArraysAndPointers();
                break;
            case 5:
                PointerArithmetic();
                break;
            case 6:
                PassingPointersToFunctions();
                break;
        	case 7:
        		ReturnPointerFunctions();
				break;
        	case 8:
        		References();
        		break;
        }
    }while(selectedOption != 0);
}