#include <iostream>

const double CHARGE_PER_SMALL_ROOM = 25;
const double CHARGE_PER_LARGE_ROOM = 35;
const double SALES_TAX = 0.06;
const int VALIDATION_DAYS = 30;

using namespace std;

int main() {
    cout << "Estimate for carpet cleaning service" << endl;
    cout << "Number of small rooms: ";

    int smallRoomsNumber;
    cin >> smallRoomsNumber;

    cout << "Number of large rooms: ";

    int largeRoomsNumber;
    cin >> largeRoomsNumber;

    cout << "Price per small room: $" << CHARGE_PER_SMALL_ROOM << endl;
    cout << "Price per large room: $" << CHARGE_PER_LARGE_ROOM << endl;

    double costWithoutTax = smallRoomsNumber * CHARGE_PER_SMALL_ROOM + largeRoomsNumber * CHARGE_PER_LARGE_ROOM;
    double tax = costWithoutTax * SALES_TAX;
    double totalCost = costWithoutTax + tax;
    cout << "Cost: $" << costWithoutTax << endl;
    cout << "Tax: $" << tax << endl;
    cout << "==============================================" << endl;
    cout << "Total estimate: $" << totalCost << endl;
    cout << "Estimate valid for " << VALIDATION_DAYS << " days.";
}
