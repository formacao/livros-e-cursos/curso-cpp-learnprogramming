#include <iostream>
#include <string>
#include <cctype>

using namespace std;

string Substitute(string message, string source, string cipher);

const string alphabet = "ABCDEFGH IJKLMNOPQRSTUVWXYZ,.:;?/}]{[=+-_1234567890abcdefghijklmnopqrstuvwxyz";
const string cipher = " Z0671342895EBRASCDFGHIJKLMNOPQTUVWXY-+_=zebrascdfghijklmnoprtuvwxy][}?/{.,;:";

int main() {
    string message, encryptedMessage, decryptedMessage;

    cout << "Type a message: ";
    getline(cin, message);

    encryptedMessage = Substitute(message, alphabet, cipher);
    decryptedMessage = Substitute(encryptedMessage, cipher, alphabet);

    cout << "Original message: " << message << endl;
    cout << "Encrypted message: " << encryptedMessage << endl;
    cout << "Decrypted message: " << decryptedMessage << endl;

    return 0;
}

string Substitute(string message, string source, string cipher) {
    string returnMessage {};
    for (size_t i = 0; i < message.length(); i++) {
        char messageChar = message.at(i);
        size_t sourceIndex = source.find(messageChar);
        if(sourceIndex != string::npos)
        {
            char cipherChar = cipher.at(sourceIndex);
            returnMessage += cipherChar;
        }
        else
        {
            returnMessage += messageChar;
        }
    }

        return returnMessage;
}