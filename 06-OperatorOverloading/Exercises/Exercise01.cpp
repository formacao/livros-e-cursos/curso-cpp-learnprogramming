//
// Created by cicero.lacerda on 05/04/2021.
//
#include "../Classes/MyString.h"
#include "Exercise01.h"

void Exercise01()
{
	MyString a("Hello");
	MyString b;
	b = a;
	b = "This is a test";
	
	MyString empty;
	MyString larry("Larry");
	MyString stooge(larry);
	
	empty.Display();
	larry.Display();
	stooge.Display();
	
	MyString aa("Hello");
	aa = MyString("Hola");
	aa = "Bonjour";
}
