//
// Created by cicero.lacerda on 13/04/2021.
//

#include <iostream>
#include "Exercise03.h"
#include "../Classes/MyString.h"

using namespace std;

void Exercise03()
{
    MyString larry("Larry");
    MyString moe("Moe");
    MyString curly;

    cout << "Enter the third stooge's first name: ";
    cin >> curly;

    cout << "The three stooges are " << larry << ", " << moe << ", and " << curly << endl;
    cout << "Enter the three stooges names separated by a space: ";
    cin >> larry >> moe >> curly;

    cout << "The three stooges are " << larry << ", " << moe << ", and " << curly << endl;
}
