//
// Created by cicero.lacerda on 12/04/2021.
//

#include <iostream>
#include "../Classes/MyString.h"
#include "Exercise02.h"

using namespace std;

void Exercise02()
{
    MyString larry("Larry");
    MyString moe("Moe");

    MyString stooge = larry;
    larry.Display();
    moe.Display();

    cout << (larry == moe) << endl;
    cout << (larry == stooge) << endl;

    larry.Display();
    MyString larry2 = -larry;
    larry2.Display();

    MyString stooges = larry + " Moe";
    stooges.Display();
}
