//
// Created by cicero.lacerda on 05/04/2021.
//

#ifndef INC_06_OPERATOROVERLOADING_MYSTRING_H
#define INC_06_OPERATOROVERLOADING_MYSTRING_H

#include <iostream>

class MyString
{
private:
	char* _str;
public:
	MyString();
	MyString(const char *const s);
	MyString(const MyString& source);
	~MyString();

	MyString &operator=(const MyString &rhs);
	MyString &operator=(MyString &&rhs);

	MyString operator-() const;
	MyString operator+(const MyString &rhs) const;
	bool operator==(const MyString &rhs) const;

	void Display() const;
	int GetLength() const;
	const char* GetStr() const;
};

std::ostream &operator<<(std::ostream &outputStream, const MyString &rightHandSide);
std::istream &operator>>(std::istream &inputStream, MyString &rightHandSide);

#endif //INC_06_OPERATOROVERLOADING_MYSTRING_H
