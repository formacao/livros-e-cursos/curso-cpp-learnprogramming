//
// Created by cicero.lacerda on 05/04/2021.
//

#include <iostream>
#include <string>
#include<cstring>
#include "MyString.h"

MyString::MyString(const char *const s) : _str(nullptr)
{
    if (s == nullptr)
    {
        _str = new char[1];
        *_str = '\0';
    } else
    {
        _str = new char[std::strlen(s) + 1];
        std::strcpy(_str, s);
    }
}

MyString::MyString() : MyString(nullptr)
{
}

MyString::MyString(const MyString &source) : MyString(source._str)
{
}

MyString::~MyString()
{
    if (_str == nullptr) std::cout << "Calling destructor on nullptr." << std::endl;
    else
    {
        std::cout << "Calling destructor on ";
        Display();
    }
    delete[] _str;
}

void MyString::Display() const
{
    std::cout << _str << " : " << GetLength() << std::endl;
}

const char *MyString::GetStr() const
{
    return _str;
}

int MyString::GetLength() const
{
    return std::strlen(_str);
}

MyString &MyString::operator=(const MyString &rhs)
{
    std::cout << "Calling copy assignment operator. " << std::endl;
    if (this != &rhs)
    {
        delete[] _str;
        _str = new char[std::strlen(rhs._str) + 1];
        std::strcpy(_str, rhs._str);
    }

    return *this;
}

MyString &MyString::operator=(MyString &&rhs)
{
    std::cout << "Calling move assignment operator." << std::endl;
    if (this != &rhs)
    {
        delete[] _str;
        _str = rhs._str;
        rhs._str = nullptr;
    }

    return *this;
}

MyString MyString::operator-() const
{
    auto numberOfChars = std::strlen(_str);
    auto *copy = new char[numberOfChars + 1];
    for (auto i = 0; i < numberOfChars; i++)
    {
        if (isupper(_str[i]))
        { copy[i] = tolower(_str[i]); }
        else
        { copy[i] = toupper(_str[i]);}
    }
    copy[numberOfChars] = '\0';
    MyString temp(copy);
    delete[] copy;

    return temp;
}

MyString MyString::operator+(const MyString &rhs) const
{
   auto numberOfChars = std::strlen(_str) + std::strlen(rhs._str);
   auto *buffer = new char[numberOfChars + 1];
   std::strcpy(buffer,_str);
   std::strcat(buffer, rhs._str);

   MyString temp(buffer);
   delete[] buffer;

   return temp;
}

bool MyString::operator==(const MyString &rhs) const
{
    return std::strcmp(_str, rhs._str) == 0;
}

std::ostream &operator<<(std::ostream &outputStream, const MyString &rightHandSide)
{
    outputStream << rightHandSide.GetStr();
    return outputStream;
}

std::istream &operator>>(std::istream &inputStream, MyString &rightHandSide)
{
    std::string buffer;
    inputStream >> buffer;
    rightHandSide = MyString(buffer.c_str());

    return inputStream;
}
