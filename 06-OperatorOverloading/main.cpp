#include <iostream>
#include "Exercises/Exercise01.h"
#include "Exercises/Exercise02.h"
#include "Exercises/Exercise03.h"

using namespace std;

int main()
{
	int option;
	do
	{
		cout << "1 - Assignment Overload" << endl;
		cout << "2 - Operators Overload as Member Function" << endl;
		cout << "3 - Stream Operators Overload" << endl;
		cout << "0 - Exit";
		cout << endl;
		cin >> option;
		switch (option)
		{
			case 1:
				Exercise01();
				break;

		    case 2:
		        Exercise02();
		        break;

		    case 3:
		        Exercise03();
		        break;
		}
		
		cout << endl;
	} while (option != 0);
	return 0;
}
