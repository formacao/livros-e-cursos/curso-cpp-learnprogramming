#include <iostream>
#include "Exercises/Exercise01.h"

using namespace std;

int main()
{
    int option;
    do
    {
        cout << "Select an options: " << endl;
        cout << "1 - Basic Inheritance" << endl;
        cout << "0 - Exit" << endl;
        cin >> option;

        switch (option)
        {
            case 1:
                Exercise01();
                break;;
        }
    }while(option != 0);

    return 0;
}
