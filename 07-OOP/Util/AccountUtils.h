//
// Created by mathe on 29/04/2021.
//

#ifndef INC_07_OOP_ACCOUNTUTILS_H
#define INC_07_OOP_ACCOUNTUTILS_H

#include <iostream>
#include "../Classes/Account.h"
#include "../Classes/SavingsAccount.h"
#include  <vector>
#include <string>

void Display(const std::vector<Account> &accounts);
void Display(const std::vector<SavingsAccount> &accounts);

void Deposit(std::vector<Account> &accounts, double amount);
void Deposit(std::vector<SavingsAccount> &accounts, double amount);

void Withdraw(std::vector<Account> &accounts, double amount);
void Withdraw(std::vector<SavingsAccount> &accounts, double amount);

#endif //INC_07_OOP_ACCOUNTUTILS_H
