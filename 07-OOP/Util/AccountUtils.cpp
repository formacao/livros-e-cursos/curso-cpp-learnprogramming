//
// Created by mathe on 29/04/2021.
//

#include <iostream>
#include "AccountUtils.h"

using namespace std;

void Display(const std::vector<Account> &accounts)
{
    cout << "\n===Accounts =================================================================" << endl;
    for (const auto &account:accounts)
    {
        cout << account << endl;
    }
}

void Display(const std::vector<SavingsAccount> &accounts)
{
    cout << "\n===Savings Accounts ==========================================================" << endl;
    for (const auto &account:accounts)
    {
        cout << account << endl;
    }
}

void Deposit(std::vector<Account> &accounts, double amount)
{
    cout << "\n===Depositing to Accounts ======================================================" << endl;
    for (auto &account:accounts)
    {
        if(account.Deposit(amount))
        {
            cout << "Deposited " << amount << " to " << account << endl;
        }
        else
        {
            cout << "Failed deposit of " << amount << " to " << account << endl;
        }
    }
}

void Deposit(std::vector<SavingsAccount> &accounts, double amount)
{
    cout << "\n===Depositing to Savings Accounts ================================================" << endl;
    for (auto &account:accounts)
    {
        if(account.Deposit(amount))
        {
            cout << "Deposited " << amount << " to " << account << endl;
        }
        else
        {
            cout << "Failed deposit of " << amount << " to " << account << endl;
        }
    }
}

void Withdraw(std::vector<Account> &accounts, double amount)
{
    cout << "\n===Withdraw from Accounts ===================================================" << endl;
    for(auto &account:accounts)
    {
        if(account.Withdraw(amount))
        {
            cout << "Withdrew " << amount << " from " << account << endl;
        }
        else
        {
            cout << "Failed Withdrawal of  " << amount << " from " << account << endl;
        }
    }
}

void Withdraw(std::vector<SavingsAccount> &accounts, double amount)
{
    cout << "\n===Withdraw from Accounts ===================================================" << endl;
    for(auto &account:accounts)
    {
        if(account.Withdraw(amount))
        {
            cout << "Withdrew " << amount << " from " << account << endl;
        }
        else
        {
            cout << "Failed Withdrawal of  " << amount << " from " << account << endl;
        }
    }
}
