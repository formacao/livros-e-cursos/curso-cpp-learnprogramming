//
// Created by cicero.lacerda on 14/04/2021.
//

#ifndef INC_07_OOP_ACCOUNT_H
#define INC_07_OOP_ACCOUNT_H

#include <iostream>
#include <string>

class Account
{
private:
    static constexpr const char *DEFAULT_NAME = "Unnamed Account";
    static constexpr double DEFAULT_BALANCE = 0;
protected:
    double Balance;
    std::string Name;
public:
    bool Deposit(double amount);
    bool Withdraw(double amount);
    double GetBalance() const;
    std::string GetName() const;

    Account(std::string name = DEFAULT_NAME, double balance = DEFAULT_BALANCE);
    ~Account();
};

std::ostream &operator << (std::ostream &outputStream, const Account &account);

#endif //INC_07_OOP_ACCOUNT_H
