//
// Created by cicero.lacerda on 14/04/2021.
//

#include<iostream>
#include "SavingsAccount.h"

SavingsAccount::SavingsAccount(std::string name, double balance, double interestRate) : Account(name, balance), InterestRate(interestRate)
{

}


bool SavingsAccount::Deposit(double amount)
{
    amount += amount * InterestRate/100;
    return Account::Deposit(amount);
}

double SavingsAccount::GetInterestRate() const
{
    return InterestRate;
}

std::ostream &operator<<(std::ostream &outputStream, const SavingsAccount &account)
{
    outputStream << "{Savings Account: " << account.GetName() << ": " << account.GetBalance() << ", " << account.GetInterestRate() << "%}";
    return outputStream;
}
