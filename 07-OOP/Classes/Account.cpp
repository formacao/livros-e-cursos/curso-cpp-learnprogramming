//
// Created by cicero.lacerda on 14/04/2021.
//
#include <iostream>
#include "Account.h"

Account::Account(std::string name, double balance) : Balance(balance), Name(name)
{
}

Account::~Account()
{

}

bool Account::Deposit(double amount)
{
    if(amount < 0) return false;
    else {
        Balance += amount;
        return true;
    }
}

bool Account::Withdraw(double amount)
{
   if(amount <= Balance)
   {
       Balance -= amount;
       return true;
   }

   return false;
}

double Account::GetBalance() const
{
    return Balance;
}

std::string Account::GetName() const
{
    return Name;
}

std::ostream &operator<<(std::ostream &outputStream, const Account &account)
{
    outputStream << "{Account: " << account.GetName() << ": " << account.GetBalance() << "}";
    return outputStream;
}
