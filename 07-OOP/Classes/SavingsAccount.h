//
// Created by cicero.lacerda on 14/04/2021.
//

#ifndef INC_07_OOP_SAVINGSACCOUNT_H
#define INC_07_OOP_SAVINGSACCOUNT_H

#include "Account.h"
#include <string>
#include <iostream>

class SavingsAccount: public Account
{
private:
    static constexpr const char *DEFAULT_NAME = "Unnamed Savings Account";
    static constexpr double DEFAULT_BALANCE =0;
    static constexpr double DEFAULT_INTEREST_RATE = 0;
protected:
    double InterestRate;
public:
    SavingsAccount(std::string name = DEFAULT_NAME, double balance = DEFAULT_BALANCE, double interestRate = DEFAULT_INTEREST_RATE );
    bool Deposit(double amount);
    double GetInterestRate() const;
};

std::ostream  &operator<<(std::ostream &outputStream, const SavingsAccount &account);

#endif //INC_07_OOP_SAVINGSACCOUNT_H
