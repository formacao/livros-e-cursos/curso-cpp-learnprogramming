//
// Created by cicero.lacerda on 14/04/2021.
//

#include <iostream>
#include <vector>
#include "../Classes/Account.h"
#include "../Classes/SavingsAccount.h"
#include "../Util/AccountUtils.h"
#include "Exercise01.h"

using namespace std;

void Exercise01()
{
    cout.precision(2);
    cout << fixed;

    vector<Account> accounts;
    accounts.push_back(Account());
    accounts.push_back(Account("Larry"));
    accounts.push_back(Account("Moe", 2000));
    accounts.push_back(Account("Curly", 5000));

    Display(accounts);
    Deposit(accounts, 1000);
    Withdraw(accounts, 2000);

    vector<SavingsAccount> savingAccounts;
    savingAccounts.push_back(SavingsAccount());
    savingAccounts.push_back(SavingsAccount("Superman"));
    savingAccounts.push_back(SavingsAccount("Batman", 2000));
    savingAccounts.push_back(SavingsAccount("Wonderwoman", 5000, 5));

    Display(savingAccounts);
    Deposit(savingAccounts, 1000);
    Withdraw(savingAccounts, 2000);
}
