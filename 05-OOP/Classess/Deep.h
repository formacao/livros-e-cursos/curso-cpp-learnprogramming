//
// Created by cicero.lacerda on 02/04/2021.
//

#ifndef INC_05_OOP_DEEP_H
#define INC_05_OOP_DEEP_H

class Deep
{
private:
	int *_data;
public:
	Deep(int data);
	Deep(const Deep &source);
	
	virtual ~Deep();
	
	int GetData() const;
};

#endif //INC_05_OOP_DEEP_H
