//
// Created by cicero.lacerda on 02/04/2021.
//
#include <iostream>
#include "Deep.h"

using namespace std;

Deep::Deep(int data)
{
	_data = new int;
	*_data = data;
}

Deep::Deep(const Deep &source) : Deep(source.GetData())
{
}

int Deep::GetData() const
{
	return *_data;
}

Deep::~Deep()
{
	delete _data;
	cout << "A deep object was destroyed" << endl;
}
