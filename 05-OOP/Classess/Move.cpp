//
// Created by cicero.lacerda on 02/04/2021.
//
#include <iostream>
#include "Move.h"

using namespace std;

MoveBase::MoveBase(int data)
{
	_data = new int;
	*_data = data;
	
	cout << "Invoked int constructor with " << data <<  endl;
}

MoveBase::MoveBase(const MoveBase &source) : MoveBase(*source._data)
{
	cout <<"Invoked copy constructor with " << *_data << endl;
}

MoveBase::~MoveBase()
{
	if(_data == nullptr) cout << "Invoked destructor for nullptr" << endl;
	else cout << "Invoked destructor for " << *_data << endl;
	
	delete _data;
}

Move::Move(int data): MoveBase(data)
{
}

Move::Move(const Move &source): MoveBase(source)
{
}

Move::Move(Move &&source) : Move(*source._data)
{
	source._data = nullptr;
}
