//
// Created by cicero.lacerda on 26/03/2021.
//

#include <string>

#ifndef INC_05_OOP_ACCOUNT_H
#define INC_05_OOP_ACCOUNT_H

class Account
{
private:
	std::string _name;
	double _balance;
public:
	void SetName(std::string name);
	const std::string&  GetName() const;
	void SetBalance(double value);
	double GetBalance() const;
	bool Deposit(double);
	bool Withdraw(double);
};

#endif //INC_05_OOP_ACCOUNT_H
