//
// Created by cicero.lacerda on 02/04/2021.
//

#ifndef INC_05_OOP_MOVE_H
#define INC_05_OOP_MOVE_H

class MoveBase {

protected:
	int *_data;
public:
	MoveBase(int data);
	MoveBase(const MoveBase &source);
	~MoveBase();
	
	int GetData() {return *_data;}
	void SetData(int data) {*_data = data;}
};

class Move : public MoveBase {
public:
	Move(int data);
	Move(const Move &source);
	Move(Move &&source);
};

#endif //INC_05_OOP_MOVE_H
