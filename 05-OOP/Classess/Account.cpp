//
// Created by cicero.lacerda on 26/03/2021.
//
#include <string>
#include "Account.h"

void Account::SetName(std::string name)
{
	this->_name = name;
}

const std::string&  Account::GetName() const
{
	return this->_name;
}

void Account::SetBalance(double value)
{
	this->_balance = value;
}

double Account::GetBalance() const
{
	return this->_balance;
}

bool Account::Deposit(double value)
{
	this->_balance += value;
	return true;
}

bool Account::Withdraw(double value)
{
	if(this->_balance >= value)
	{
		this->_balance -= value;
		return true;
	}
	
	return false;
}
