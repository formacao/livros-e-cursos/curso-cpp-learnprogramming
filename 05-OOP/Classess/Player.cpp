//
// Created by cicero.lacerda on 26/03/2021.
//
#include <iostream>
#include "Player.h"

using namespace std;

int Player::_numPlayers(0);

Player::Player(const Player &source): Player(source.GetName(), source.GetHealth(), source.GetXp())
{
	cout << "Made a copy of " << source.GetName() << endl;
}

Player::Player(const string &name, int health,  int xp) : _name(name), _health(health), _xp(xp)
{
	_numPlayers++;
}

Player::~Player()
{
	_numPlayers--;
	cout << "Player with name " << _name << " was destroyed!" << endl;
}

void Player::Talk(std::string textToSay)
{
	cout << this->_name << " says: " << textToSay << endl;
}

const string &Player::GetName() const
{
	return _name;
}

void Player::SetName(const string &name)
{
	_name = name;
}

int Player::GetHealth() const
{
	return _health;
}

void Player::SetHealth(int health)
{
	_health = health;
}

int Player::GetXp() const
{
	return _xp;
}

void Player::SetXp(int xp)
{
	_xp = xp;
}

int Player::GetNumPlayers()
{
	return _numPlayers;
}

