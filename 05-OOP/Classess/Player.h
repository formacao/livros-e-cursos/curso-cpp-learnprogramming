//
// Created by cicero.lacerda on 26/03/2021.
//

#include <string>

#ifndef INC_05_OOP_PLAYER_H
#define INC_05_OOP_PLAYER_H

class Player
{
private:
	std::string _name;
	int _health;
	int _xp;
	
	static int _numPlayers;

public:
	Player(const Player &source);
	Player(const std::string &name = "None", int health = 0, int xp = 0);
	~Player();
	
	void Talk(std::string);
	bool IsDead();
	
	const std::string &GetName() const;
	void SetName(const std::string &name);
	int GetHealth() const;
	void SetHealth(int health);
	int GetXp() const;
	void SetXp(int xp);
	
	static int GetNumPlayers();
};

#endif //INC_05_OOP_PLAYER_H
