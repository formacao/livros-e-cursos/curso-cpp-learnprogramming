//
// Created by cicero.lacerda on 02/04/2021.
//

#ifndef INC_05_OOP_EXERCISE04_H
#define INC_05_OOP_EXERCISE04_H

#include <vector>
#include "../Classess/Move.h"

using namespace std;

void Exercise04();
void Compare(vector<MoveBase> &vec, vector<Move> &vecMove, int data);

#endif //INC_05_OOP_EXERCISE04_H
