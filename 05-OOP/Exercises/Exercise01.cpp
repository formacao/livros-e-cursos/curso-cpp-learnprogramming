//
// Created by cicero.lacerda on 02/04/2021.
//
#include <iostream>
#include "../Classess/Player.h"
#include "../Classess/Account.h"
#include "Exercise01.h"

using namespace std;

void Exercise01()
{
	Player anonymous;
	
	Player cicero;
	cicero.SetName("Cícero");
	cicero.SetHealth(100);
	cicero.SetXp(28);
	
	cicero.Talk("Hello there");
	
	Player *enemy = new Player;
	enemy->SetName("Ghost");
	enemy->SetHealth(12);
	enemy->SetXp(2);
	
	enemy->Talk("boo");
	delete enemy;
	
	Account *ciceroAccount = new Account;
	ciceroAccount->SetName("Cicero's Account");
	ciceroAccount->SetBalance(1000);
	
	ciceroAccount->Deposit(300);
	ciceroAccount->Withdraw(700);
	
	cout << ciceroAccount->GetBalance() << " BRL " << endl;
	delete ciceroAccount;
	cout << endl;
}

