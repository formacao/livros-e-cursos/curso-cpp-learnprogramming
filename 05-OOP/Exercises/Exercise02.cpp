//
// Created by cicero.lacerda on 02/04/2021.
//
#include <iostream>
#include "../Classess/Player.h"
#include "Exercise02.h"

using namespace std;

void Exercise02()
{
	auto *player = new Player("Cícero", 100, 13);
	
	cout << "Name: " << player->GetName() << endl;
	cout << "Hp: " << player->GetHealth() << endl;
	cout << "Xp: " << player->GetXp() << endl;
	
	delete player;
	cout << endl;
}
