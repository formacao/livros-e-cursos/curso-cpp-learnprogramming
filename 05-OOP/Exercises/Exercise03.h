//
// Created by cicero.lacerda on 02/04/2021.
//

#ifndef INC_05_OOP_EXERCISE03_H
#define INC_05_OOP_EXERCISE03_H

#include "../Classess/Player.h"
#include "../Classess/Deep.h"

void Exercise03();
void DisplayPlayer(Player p);
void DisplayData(Deep deep);


#endif //INC_05_OOP_EXERCISE03_H
