//
// Created by cicero.lacerda on 02/04/2021.
//
#include <vector>
#include <iostream>
#include "Exercise04.h"
#include "../Classess/Move.h"

using namespace std;

void Exercise04()
{
	vector<MoveBase> vecMoveBase;
	vector<Move> vecMove;
	for (int i = 10; i <= 100; i += 10)
	{
		Compare(vecMoveBase, vecMove, i);
		cout << "====================================" << endl;
	}
	
}

void Compare(vector<MoveBase> &vec, vector<Move> &vecMove, int data)
{
	cout << "Adding MoveBase with " << data << endl;
	vec.push_back(MoveBase(data));
	cout << "Adding Move with " << data << endl;
	vecMove.push_back(Move(data));
}
