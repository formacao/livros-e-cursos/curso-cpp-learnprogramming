//
// Created by cicero.lacerda on 03/04/2021.
//

#include <iostream>
#include "../Classess/Player.h"
#include "Exercise05.h"

using namespace std;

void DisplayNumberOfPlayers()
{
	cout << "Active Players: " << Player::GetNumPlayers() << endl;
}

void Exercise05()
{
	DisplayNumberOfPlayers();
	Player john;
	DisplayNumberOfPlayers();
	auto *cicero = new Player("Cícero");
	DisplayNumberOfPlayers();
	delete cicero;
	DisplayNumberOfPlayers();
}
