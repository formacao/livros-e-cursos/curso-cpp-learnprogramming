//
// Created by cicero.lacerda on 02/04/2021.
//
#include <iostream>
#include "Exercise03.h"
#include "../Classess/Player.h"
#include "../Classess/Deep.h"

using namespace std;

void Exercise03()
{
	auto player = Player("Cícero");
	auto deep = Deep(30);
	DisplayPlayer(player);
	DisplayData(deep);
	
	cout <<"Original deep still has his data: " << deep.GetData() << endl;
}

void DisplayPlayer(Player p)
{
	cout << "Name: " << p.GetName() << endl;
	cout << "Health: " << p.GetHealth() << endl;
	cout << "Xp: " << p.GetXp() << endl;
}

void DisplayData(Deep deep)
{
	cout << "Data: " << deep.GetData() << endl;
}