#include <iostream>
#include "Exercises/Exercise01.h"
#include "Exercises/Exercise02.h"
#include "Exercises/Exercise03.h"
#include "Exercises/Exercise04.h"
#include "Exercises/Exercise05.h"

using namespace std;

int main()
{
	auto option{0};
	do
	{
		cout << "Choose an option:" << endl;
		cout << "1 - Acessing Class Members" << endl;
		cout << "2 - Constructors and Destructors" << endl;
		cout << "3 - Copy Constructor" << endl;
		cout << "4 - Move Constructor" << endl;
		cout << "5 - Static Members" << endl;
		cout << "0 - Exit" << endl;
		cin >> option;
		
		switch (option)
		{
			case 1:
				Exercise01();
				break;
			case 2:
				Exercise02();
				break;
			case 3:
				Exercise03();
				break;
			case 4:
				Exercise04();
				break;
			case 5:
				Exercise05();
				break;
		}
	}while(option != 0);
	return 0;
}
