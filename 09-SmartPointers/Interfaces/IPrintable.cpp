//
// Created by mathe on 04/05/2021.
//

#include "IPrintable.h"

std::ostream &operator<<(std::ostream &outputStream, const IPrintable &printable)
{
    printable.Print(outputStream);
    return outputStream;
}
