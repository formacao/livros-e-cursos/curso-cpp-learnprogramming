//
// Created by mathe on 05/05/2021.
//

#ifndef INC_09_SMARTPOINTERS_EXERCISES_H
#define INC_09_SMARTPOINTERS_EXERCISES_H

void Exercise01();
void Exercise02();
void Exercise03();
void Exercise04();

#endif //INC_09_SMARTPOINTERS_EXERCISES_H
