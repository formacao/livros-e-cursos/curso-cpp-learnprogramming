//
// Created by mathe on 05/05/2021.
//

#include <iostream>
#include <vector>
#include "../Classes/Accounts.h"
#include "Exercises.h"

using namespace std;

class Test
{
private:
    int _data;
public:
    Test() : Test(0)
    {};

    Test(int data) : _data(data)
    { cout << "Test constructor(" << _data << ")" << endl; }

    int GetData() const
    { return _data; }

    ~Test()
    { cout << "Test destructor(" << _data << ")" << endl; }

};

void Exercise01()
{
    unique_ptr<Test> t1 = make_unique<Test>(100);
    unique_ptr<Test> t2 = move(t1);

    if (!t1) cout << "t1 is nullptr" << endl;

    unique_ptr<Account> a1 = make_unique<CheckingAccount>();
    cout << *a1 << endl;

    vector<unique_ptr<Account>> accounts;
    accounts.push_back(make_unique<TrustAccount>());
    accounts.push_back(make_unique<SavingsAccount>());
    accounts.push_back(make_unique<CheckingAccount>());
    accounts.push_back(make_unique<Account>());

    for (const auto &account: accounts)
        cout << *account << endl;

}

void DisplayUseCount(int useCount)
{
    cout << "Use Count: " << useCount << endl;
}

void DisplayTestUseCount(shared_ptr<Test> ptr)
{
    DisplayUseCount(ptr.use_count());
}

void Exercise02()
{
    auto p1 = make_shared<int>(100);
    DisplayUseCount(p1.use_count());

    shared_ptr<int> p2{p1};
    DisplayUseCount(p1.use_count());
    DisplayUseCount(p2.use_count());

    p1.reset();
    DisplayUseCount(p1.use_count());
    DisplayUseCount(p2.use_count());

    cout << "\n=============================================" << endl;

    auto ptr = make_shared<Test>(100);
    DisplayTestUseCount(ptr);
    DisplayUseCount(ptr.use_count());
    {
        shared_ptr<Test> ptr1 = ptr;
        DisplayUseCount(ptr.use_count());
        {
            shared_ptr<Test> ptr2 = ptr;
            DisplayUseCount(ptr.use_count());

            ptr.reset();
        }
        DisplayUseCount(ptr.use_count());
    }
    DisplayUseCount(ptr.use_count());

    cout << "\n=================================================" << endl;

    shared_ptr<Account> acc1 = make_shared<TrustAccount>();
    shared_ptr<Account> acc2 = make_shared<SavingsAccount>();
    shared_ptr<Account> acc3 = make_shared<CheckingAccount>();

    vector<shared_ptr<Account>> accounts;
    accounts.push_back(acc1);
    accounts.push_back(acc2);
    accounts.push_back(acc3);

    for (const auto &account:accounts)
    {
        cout << *account << endl;
        DisplayUseCount(account.use_count());
    }
}

class B;

class A
{
    shared_ptr<B> _b;
public:
    void SetB(shared_ptr<B> &b) {_b = b;}
    A() {cout << "A Constructor" << endl;}
    ~A() {cout << "A Destructor" << endl;}
};

class B
{
    weak_ptr<A> _a;
public:
    void SetA(shared_ptr<A> &a) {_a = a;}
    B() {cout << "B Constructor" << endl;}
    ~B(){cout << "B Destructor" << endl;}
};

void Exercise03()
{
    shared_ptr<A> a = make_shared<A>();
    shared_ptr<B> b = make_shared<B>();

    a->SetB(b);
    b->SetA(a);
}

void MyDeleter(Test *ptr)
{
    cout << "On my custom deleter" << endl;

    delete ptr;
}

void Exercise04()
{
    shared_ptr<Test> test {new Test(), MyDeleter};
}
