//
// Created by mathe on 04/05/2021.
//

#ifndef INC_08_POLYMORPHISM_ACCOUNTS_H
#define INC_08_POLYMORPHISM_ACCOUNTS_H

#include <iostream>
#include "../Interfaces/IPrintable.h"

using namespace std;

class Account: public IPrintable{
public:
    virtual void Withdraw(double amount);
    void Print(ostream &outputStream) const override;
    virtual ~Account();
};

class CheckingAccount : public Account {
public:
    virtual void Withdraw(double amount) override;
    void Print(ostream &outputStream) const override;

    virtual ~CheckingAccount() override
    {
        cout << "CheckingAccount::Destructor" << endl;
    }
};

class SavingsAccount: public Account {
public:
    virtual void Withdraw(double amount) override;
    void Print(ostream &outputStream) const override;

    virtual ~SavingsAccount() override
    {
        cout << "SavingsAccount::Destructor" << endl;
    }
};

class TrustAccount: public Account {
public:
    virtual void Withdraw(double amount) override;
    void Print(ostream &outputStream) const override;

    virtual ~TrustAccount() override
    {
        cout << "TrustAccount::Destructor" << endl;
    }
};

#endif //INC_08_POLYMORPHISM_ACCOUNTS_H
