cmake_minimum_required(VERSION 3.19)
project(09_SmartPointers)

set(CMAKE_CXX_STANDARD 20)

add_executable(09_SmartPointers main.cpp Exercises/Exercises.cpp Exercises/Exercises.h Classes/Accounts.h Classes/Accounts.cpp Interfaces/IPrintable.h
        Interfaces/IPrintable.cpp)