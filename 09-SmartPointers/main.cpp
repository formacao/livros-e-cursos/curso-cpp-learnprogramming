#include <iostream>
#include "Exercises/Exercises.h"

using namespace std;

int main()
{
    auto option = 0;
    do
    {
        cout << "Select an option:" << endl;
        cout << "1 - Unique Pointers" << endl;
        cout << "2 - Shared Pointers" << endl;
        cout << "3 - Weak Pointers" << endl;
        cout << "4 - Custom Deleters" << endl;
        cout << "0 - Exit" << endl;

        cin >> option;

        switch (option)
        {
            case 1:
                Exercise01();
                break;

            case 2:
                Exercise02();
                break;

            case 3:
                Exercise03();
                break;

            case 4:
                Exercise04();
                break;
        }
    } while (option != 0);
    return 0;
}
